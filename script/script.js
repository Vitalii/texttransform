var txtSet = {
	fontFamily: ["source-sans-pro", "karmina", "voltage"],
	fontName: ["Source Sans", "Karmina", "Voltage"],
	fontStyle: ["normal", "bold", "italic"],
	bg: ["#4990e2", "#1fcc6d", "#f1c40f", "#34495e", "#ecf0f1"]
};


var abBtn = document.querySelectorAll(".ab_btn"),
	fontBtn = document.querySelectorAll(".font_btn"),
	fontName = document.querySelector(".font_name"),
	fontNameStyle = document.querySelector(".font_name_style"),
	fontNameItal = document.querySelector(".font_name_ital"),
	bgCircle = document.querySelectorAll(".bg_circle"),
	textprevBlock = document.querySelector(".textprev_block"),
	typefaceActive = document.querySelector('.typeface_active'),
	loremText = document.querySelector(".lorem_text");


/*Choose font-family*/
for (var i = 0; i < abBtn.length; i++) {
	;(function(i){
		abBtn[i].addEventListener('click', function() {
			if(i === 0){
				typefaceActive.style.left = abBtn[i].offsetLeft + "px";
				loremText.style.fontFamily = txtSet.fontFamily[0];
				fontName.innerHTML = txtSet.fontName[0];
			}else if(i === 1){
				typefaceActive.style.left = abBtn[i].offsetLeft + "px";
				loremText.style.fontFamily = txtSet.fontFamily[1];
				fontName.innerHTML = txtSet.fontName[1];
			}else {
				typefaceActive.style.left = abBtn[i].offsetLeft + "px";
				loremText.style.fontFamily = txtSet.fontFamily[2];
				fontName.innerHTML = txtSet.fontName[2];
			}
			
		});
	})(i);
};



/*Choose font styling*/
for (var i = 0; i < fontBtn.length; i++) {
	;(function(i){
		fontBtn[i].addEventListener('click', function() {
			if (i === 0) {
				loremText.style.fontWeight = txtSet.fontStyle[0];
				loremText.style.fontStyle = txtSet.fontStyle[0];
				fontNameStyle.innerHTML = txtSet.fontStyle[0];
				fontNameItal.innerHTML = "";
				fontBtn[i].style.color = "#000000";
				fontBtn[1].style.color = "#a6a6a6";
				fontBtn[2].style.color = "#a6a6a6";
			}else if (i === 1) {
				if (loremText.style.fontWeight === txtSet.fontStyle[1]) {
					fontBtn[i].style.color = "#a6a6a6";
					fontBtn[0].style.color = "#000000";
					loremText.style.fontWeight = txtSet.fontStyle[0];
					fontNameStyle.innerHTML = txtSet.fontStyle[0];
				}else{
					fontBtn[i].style.color = "#000000";
					fontBtn[0].style.color = "#a6a6a6";
					loremText.style.fontWeight = txtSet.fontStyle[1];
					fontNameStyle.innerHTML = txtSet.fontStyle[1];
				};
			}else{
				if (loremText.style.fontStyle === txtSet.fontStyle[2]) {
					fontBtn[i].style.color = "#a6a6a6";
					loremText.style.fontStyle = txtSet.fontStyle[0]
					fontNameItal.innerHTML = "";
				}else{
					fontBtn[i].style.color = "#000000";
					loremText.style.fontStyle = txtSet.fontStyle[2];
					fontNameItal.innerHTML = txtSet.fontStyle[2];
				};
			};
		});
	})(i);
}


/*Choose background color*/
for (var i = 0; i < bgCircle.length; i++) {
	;(function(i){
		bgCircle[i].addEventListener('click', function() {
			if (i === bgCircle.length-1) {
				textprevBlock.style.background = txtSet.bg[i];
				loremText.style.color = '#000000';
			}else{
				textprevBlock.style.background = txtSet.bg[i];
				loremText.style.color = '#ffffff';
			};
			for (var z = 0; z < bgCircle.length; z++) {
				if (z === i) {
					bgCircle[z].style.fontSize = 18 + 'px';
				}else {
					bgCircle[z].style.fontSize = 0 + 'px';
				};
			};
		});
	})(i);
}